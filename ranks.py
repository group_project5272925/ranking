def parse_line(line: str) -> list:

    parts = line.split()  
    name, marks = parts[0], parts[1:]  
    marks = [int(mark) for mark in marks if mark]  
    return [name, marks]  
def load_data(mark_file: str) -> list:
    return [parse_line(line.strip()) for line in open(mark_file)]
def is_rankable(list_a: list[int], list_b: list[int]) -> bool:
    return all(a > b for a, b in zip(list_a, list_b))
def rank(list_stud: list) -> tuple[list, list]:
    ranked_students = []
    not_rankable = []
    num_students = len(list_stud)
    
    for i in range(num_students):
        student_a, marks_a = list_stud[i]
        for j in range(i + 1, num_students):
            student_b, marks_b = list_stud[j]
            if is_rankable(marks_a, marks_b):
                ranked_students.append((student_a, student_b))
            elif is_rankable(marks_b, marks_a):
                ranked_students.append((student_b, student_a))
            else:
                not_rankable.append((student_a, student_b))
    return [' > '.join(pair) for pair in ranked_students], ['>'.join(pair) for pair in not_rankable]
    
   # return ranked_students, not_rankable
print(rank(load_data("marks.txt")))
