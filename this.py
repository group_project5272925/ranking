def parse_line(line: str) -> list:

    parts = line.split()  
    name, marks = parts[0], parts[1:]  
    marks = [int(mark) for mark in marks if mark]  
    return [name, marks]  
def load_data(mark_file: str) -> list:
    return [parse_line(line.strip()) for line in open(mark_file)]
def is_rankable(list_a: list[int], list_b: list[int]) -> bool:
    return all(a > b or a < b for a, b in zip(list_a, list_b))
    
def rank(list_stud: list) -> tuple[list, list]:
    ranked_students = []
    not_rankable = []
    num_students = len(list_stud)
    
    for i in range(num_students):
        student_a, marks_a = list_stud[i]
        for j in range(i + 1, num_students):
            student_b, marks_b = list_stud[j]
            if is_rankable(marks_a, marks_b) :
                ranked_students.append((student_a))
#            elif is_rankable(marks_b, marks_a) :
#               ranked_students.append((student_b, student_b))
            else:
                not_rankable.append((student_a))
#            list_stud.pop(i)
    
    return ranked_students, not_rankable

ranked, not_rankable = rank(load_data("marks.txt"))
print("Ranked students:", ranked)
print("Not rankable pairs:", not_rankable)
