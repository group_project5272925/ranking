class Student:
    def __init__(self, data:str):
        self.name, *raw_marks= data.strip().split()
        self.marks = [int(_) for _ in raw_marks]
        self.topper= False
        self.trailers = set()
def __repr__(self)->str:
    def form(mark : int)->str:
        return f'{mark: 4}'
    return f'{self.name}:{" ".join([form(_) for _ in self_marks])}'
def __gt__(self, other)->bool:
    return all(a>b for a, b in zip(self.marks, other.marks))
